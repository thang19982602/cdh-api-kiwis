package neo.nbd.utils;

import neo.nbd.dto.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ConvertData {

    public final static String URL = "https://cdh.vnmha.gov.vn/KiWIS/KiWIS?service=kisters&type=queryServices&request=getTimeseriesValues&datasource=0&format=json";
    public final static RestTemplate template = new RestTemplate();
    public static ResponseData[] result = null;

    public static List<InputDataHoChua> readFileInputHoChua(String url) {
        List<InputDataHoChua> list = new ArrayList<>();
        try {
            FileInputStream file = new FileInputStream(new File(url));
            // Khởi tạo workbook cho tệp xlsx
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            // Lấy worksheet đầu tiên trong workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            int index = 0;
            Double day = sheet.getRow(1).getCell(1).getNumericCellValue();
            // Duyệt qua từng row
            Iterator<Row> rowIterator = sheet.rowIterator();

            while (rowIterator.hasNext()) {
                if (index >= 3) {
                    InputDataHoChua d = new InputDataHoChua();
                    Row row = sheet.getRow(index);
                    d.setDayNumber(day);
                    d.setName(row.getCell(0).getCellType() == Cell.CELL_TYPE_STRING ? row.getCell(0).getStringCellValue() : String.valueOf(row.getCell(0).getNumericCellValue()));
                    d.setMucNuoc(row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING ? Double.valueOf(row.getCell(1).getStringCellValue()) : row.getCell(1).getNumericCellValue());
                    d.setQXa(row.getCell(2).getCellType() == Cell.CELL_TYPE_STRING ? Double.valueOf(row.getCell(2).getStringCellValue()) : row.getCell(2).getNumericCellValue());
                    d.setQDen(row.getCell(3).getCellType() == Cell.CELL_TYPE_STRING ? Double.valueOf(row.getCell(3).getStringCellValue()) : row.getCell(3).getNumericCellValue());
                    list.add(d);
                }
                index++;
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List<InputDaTaMua> readFileInputMua(String url) {
        List<InputDaTaMua> list = new ArrayList<>();
        try {
            FileInputStream file = new FileInputStream(new File(url));
            // Khởi tạo workbook cho tệp xlsx
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            // Lấy worksheet đầu tiên trong workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            int index = 0;
            Double day = sheet.getRow(1).getCell(1).getNumericCellValue();
            // Duyệt qua từng row
            Iterator<Row> rowIterator = sheet.rowIterator();

            while (rowIterator.hasNext()) {
                if (index >= 3) {
                    InputDaTaMua d = new InputDaTaMua();
                    Row row = sheet.getRow(index);
                    d.setDayNumber(day);
                    d.setName(row.getCell(0).getCellType() == Cell.CELL_TYPE_STRING ? row.getCell(0).getStringCellValue() : String.valueOf(row.getCell(0).getNumericCellValue()));
                    d.setMua(row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING ? Double.valueOf(row.getCell(1).getStringCellValue()) : row.getCell(1).getNumericCellValue());
                    list.add(d);
                }
                index++;
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List<InputDataMucNuoc> readFileInputMucNuoc(String url) {
        List<InputDataMucNuoc> list = new ArrayList<>();
        try {
            FileInputStream file = new FileInputStream(new File(url));
            // Khởi tạo workbook cho tệp xlsx
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            // Lấy worksheet đầu tiên trong workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            int index = 0;
            Double day = sheet.getRow(1).getCell(1).getNumericCellValue();
            // Duyệt qua từng row
            Iterator<Row> rowIterator = sheet.rowIterator();

            while (rowIterator.hasNext()) {
                if (index >= 3) {
                    InputDataMucNuoc d = new InputDataMucNuoc();
                    Row row = sheet.getRow(index);
                    d.setDayNumber(day);
                    d.setName(row.getCell(0).getCellType() == Cell.CELL_TYPE_STRING ? row.getCell(0).getStringCellValue() : String.valueOf(row.getCell(0).getNumericCellValue()));
                    d.setMucNuoc(row.getCell(1).getCellType() == Cell.CELL_TYPE_STRING ? Double.valueOf(row.getCell(1).getStringCellValue()) : row.getCell(1).getNumericCellValue());
                    list.add(d);
                }
                index++;
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }


    public static List<String> getUrlMucNuoc(String URL, String fromDate, String toDate, List<InputDataHoChua> inputDataHoChuaList) {
        SimpleDateFormat outDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat inPutFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date inputDate = new Date();
        try {
            inputDate = inPutFormat.parse(fromDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String fromDateOutPut = outDateFormat.format(inputDate);
        try {
            inputDate = inPutFormat.parse(toDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String toDateOutPut = outDateFormat.format(inputDate);
        List<String> list = new ArrayList<>();
        System.out.println("--" + inputDataHoChuaList);
        for (int i = 0; i < inputDataHoChuaList.size(); i++) {
            Double rusult = inputDataHoChuaList.get(i).getMucNuoc();
            int IntValue = (int) Math.round(rusult);
            String ts_id = "&ts_id=" + IntValue;
            String newTs_id = ts_id.concat(ts_id + "&from=" + fromDateOutPut + "&to=" + toDateOutPut);
            String newUrl = URL.concat(newTs_id);
            list.add(newUrl);
        }
        return list;
    }

    public static List<String> getUrlQXa(String URL, String fromDate, String toDate, List<InputDataHoChua> inputDataHoChuaList) {
        SimpleDateFormat outDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat inPutFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date inputDate = new Date();
        try {
            inputDate = inPutFormat.parse(fromDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String fromDateOutPut = outDateFormat.format(inputDate);
        try {
            inputDate = inPutFormat.parse(toDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String toDateOutPut = outDateFormat.format(inputDate);
        List<String> list = new ArrayList<>();
        System.out.println("--" + inputDataHoChuaList);
        for (int i = 0; i < inputDataHoChuaList.size(); i++) {
            Double rusult = inputDataHoChuaList.get(i).getQXa();
            int IntValue = (int) Math.round(rusult);
            String ts_id = "&ts_id=" + IntValue;
            String newTs_id = ts_id.concat(ts_id + "&from=" + fromDateOutPut + "&to=" + toDateOutPut);
            String newUrl = URL.concat(newTs_id);
            list.add(newUrl);
        }
        return list;
    }

    public static List<String> getUrlQden(String URL, String fromDate, String toDate, List<InputDataHoChua> inputDataHoChuaList) {
        SimpleDateFormat outDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat inPutFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date inputDate = new Date();
        try {
            inputDate = inPutFormat.parse(fromDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String fromDateOutPut = outDateFormat.format(inputDate);
        try {
            inputDate = inPutFormat.parse(toDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String toDateOutPut = outDateFormat.format(inputDate);
        List<String> list = new ArrayList<>();
        System.out.println("--" + inputDataHoChuaList);
        for (int i = 0; i < inputDataHoChuaList.size(); i++) {
            Double rusult = inputDataHoChuaList.get(i).getQDen();
            int IntValue = (int) Math.round(rusult);
            String ts_id = "&ts_id=" + IntValue;
            String newTs_id = ts_id.concat(ts_id + "&from=" + fromDateOutPut + "&to=" + toDateOutPut);
            String newUrl = URL.concat(newTs_id);
            list.add(newUrl);
        }
        return list;
    }

    public static List<String> getUrlMua(String URL, String fromDate, String toDate, List<InputDaTaMua> inputDataMuaList) {
        SimpleDateFormat outDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat inPutFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date inputDate = new Date();
        try {
            inputDate = inPutFormat.parse(fromDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String fromDateOutPut = outDateFormat.format(inputDate);
        try {
            inputDate = inPutFormat.parse(toDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String toDateOutPut = outDateFormat.format(inputDate);
        List<String> list = new ArrayList<>();
        System.out.println("--" + inputDataMuaList);
        for (int i = 0; i < inputDataMuaList.size(); i++) {
            Double rusult = inputDataMuaList.get(i).getMua();
            int IntValue = (int) Math.round(rusult);
            String ts_id = "&ts_id=" + IntValue;
            String newTs_id = ts_id.concat(ts_id + "&from=" + fromDateOutPut + "&to=" + toDateOutPut);
            String newUrl = URL.concat(newTs_id);
            list.add(newUrl);
        }
        return list;
    }

    public static List<String> getUrlMucNuocs(String URL, String fromDate, String toDate, List<InputDataMucNuoc> inputDataMucNuocList) {
        SimpleDateFormat outDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat inPutFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date inputDate = new Date();
        try {
            inputDate = inPutFormat.parse(fromDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String fromDateOutPut = outDateFormat.format(inputDate);
        try {
            inputDate = inPutFormat.parse(toDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String toDateOutPut = outDateFormat.format(inputDate);
        List<String> list = new ArrayList<>();
        System.out.println("--" + inputDataMucNuocList);
        for (int i = 0; i < inputDataMucNuocList.size(); i++) {
            Double rusult = inputDataMucNuocList.get(i).getMucNuoc();
            int IntValue = (int) Math.round(rusult);
            String ts_id = "&ts_id=" + IntValue;
            String newTs_id = ts_id.concat(ts_id + "&from=" + fromDateOutPut + "&to=" + toDateOutPut);
            String newUrl = URL.concat(newTs_id);
            list.add(newUrl);
        }

        return list;
    }

    //check exit dataHoChua
    public static DataHoChua checkExitDataHoChua(List<DataHoChua> lstData, String stringDate, String stringTime) {
        for (DataHoChua o : lstData) {
            if (stringDate.equals(o.getDateHoChua())
                    && stringTime.equals(o.getTimeHoChua())) {
                return o;
            }
        }
        return new DataHoChua(stringDate, stringTime, "0");
    }

    //check exit dataMua
    public static DataMua checkExitDataMua(List<DataMua> lstData, String stringDate, String stringTime) {
        for (DataMua o : lstData) {
            if (stringDate.equals(o.getDateMua())
                    && stringTime.equals(o.getTimeMua())) {
                return o;
            }
        }
        return new DataMua(stringDate, stringTime, "0");
    }

    // check exit dataMucNuoc
    public static DataMucNuoc checkExitDataMucNuoc(List<DataMucNuoc> lstData, String stringDate, String stringTime) {
        for (DataMucNuoc o : lstData) {
            if (stringDate.equals(o.getDateMucNuoc()) && stringTime.equals(o.getTimeMucNuoc())) {
                return o;
            }
        }
        return new DataMucNuoc(stringDate, stringTime, "0");
    }

    // lấy danh sách mực mước
    public static List<DataHoChua> getDataMucNuocAPI(List<String> listDate, List<String> listTime, String formDate, String toDate, List<InputDataHoChua> inputDataHoChuaList) {
        List<DataHoChua> dataHoChuas = new ArrayList<>();
        List<DataHoChua> endData = new ArrayList<>();
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'+07:00'");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy'T'HH:mm:ss");
        Date date = null;
        for (int j = 0; j < getUrlMucNuoc(URL, formDate, toDate, inputDataHoChuaList).size(); j++) {
            result = template.getForObject(getUrlMucNuoc(URL, formDate, toDate, inputDataHoChuaList).get(j), ResponseData[].class);
            for (int i = 0; i < result[0].getData().size(); i++) {
                try {
                    date = inputFormat.parse(result[0].getData().get(i).get(0));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String formattedDate = outputFormat.format(date);
                DataHoChua d = new DataHoChua(formattedDate.split("T")[0], formattedDate.split("T")[1], result[0].getData().get(i).get(1));
                dataHoChuas.add(d);
            }
            for (String stringDate : listDate) {
                for (String stringTime : listTime) {
                    DataHoChua dataHoChuaAdd = checkExitDataHoChua(dataHoChuas, stringDate, stringTime);
                    endData.add(dataHoChuaAdd);
                }
            }
            //lam moi datas sau đó check tiep du lieu của ho tiep theo
            dataHoChuas = new ArrayList<>();
        }
        return endData;
    }

    // lấy danh sách q xả
    public static List<DataHoChua> getDataQXaAPI(List<String> listDate, List<String> listTime, String formDate, String toDate, List<InputDataHoChua> inputDataHoChuaList) {
        List<DataHoChua> dataHoChuas = new ArrayList<>();
        List<DataHoChua> endData = new ArrayList<>();
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'+07:00'");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy'T'HH:mm:ss");
        Date date = null;
        for (int j = 0; j < getUrlQXa(URL, formDate, toDate, inputDataHoChuaList).size(); j++) {
            result = template.getForObject(getUrlQXa(URL, formDate, toDate, inputDataHoChuaList).get(j), ResponseData[].class);
            for (int i = 0; i < result[0].getData().size(); i++) {
                try {
                    date = inputFormat.parse(result[0].getData().get(i).get(0));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String formattedDate = outputFormat.format(date);
                DataHoChua d = new DataHoChua(formattedDate.split("T")[0], formattedDate.split("T")[1], result[0].getData().get(i).get(1));
                dataHoChuas.add(d);
            }
            for (String stringDate : listDate) {
                for (String stringTime : listTime) {
                    DataHoChua dataHoChuaAdd = checkExitDataHoChua(dataHoChuas, stringDate, stringTime);
                    endData.add(dataHoChuaAdd);
                }
            }
            dataHoChuas = new ArrayList<>();
        }
        return endData;
    }

    // lấy danh sách q đến
    public static List<DataHoChua> getDataQDenAPI(List<String> listDate, List<String> listTime, String formDate, String toDate, List<InputDataHoChua> inputDataHoChuaList) {
        List<DataHoChua> dataHoChuas = new ArrayList<>();
        List<DataHoChua> endData = new ArrayList<>();
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'+07:00'");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy'T'HH:mm:ss");
        Date date = null;
        for (int j = 0; j < getUrlQden(URL, formDate, toDate, inputDataHoChuaList).size(); j++) {
            result = template.getForObject(getUrlQden(URL, formDate, toDate, inputDataHoChuaList).get(j), ResponseData[].class);
            for (int i = 0; i < result[0].getData().size(); i++) {
                try {
                    date = inputFormat.parse(result[0].getData().get(i).get(0));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String formattedDate = outputFormat.format(date);
                DataHoChua d = new DataHoChua(formattedDate.split("T")[0], formattedDate.split("T")[1], result[0].getData().get(i).get(1));
                dataHoChuas.add(d);
            }
            for (String stringDate : listDate) {
                for (String stringTime : listTime) {
                    DataHoChua dataHoChuaAdd = checkExitDataHoChua(dataHoChuas, stringDate, stringTime);
                    endData.add(dataHoChuaAdd);
                }
            }
            dataHoChuas = new ArrayList<>();
        }
        return endData;
    }

    //check data api
    //kiem tra xem nhung gio nao thieu thi set value = 0
    public static List<DataMua> getDataMuaAPI(List<String> listDate, List<String> listTime, String formDate, String toDate, List<InputDaTaMua> inputDataMuaList) {
        List<DataMua> dataMuas = new ArrayList<>();
        List<DataMua> endData = new ArrayList<>();
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'+07:00'");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy'T'HH:mm:ss");
        Date date = null;
        for (int j = 0; j < getUrlMua(URL, formDate, toDate, inputDataMuaList).size(); j++) {
            result = template.getForObject(getUrlMua(URL, formDate, toDate, inputDataMuaList).get(j), ResponseData[].class);
            for (int i = 0; i < result[0].getData().size(); i++) {
                try {
                    date = inputFormat.parse(result[0].getData().get(i).get(0));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String formattedDate = outputFormat.format(date);
                DataMua d = new DataMua(formattedDate.split("T")[0], formattedDate.split("T")[1], result[0].getData().get(i).get(1));
                dataMuas.add(d);
            }
            //check date and time
            //neu thieu date va time thi them vao va set value = 0 cho nhung time do
            for (String stringDate : listDate) {
                for (String stringTime : listTime) {
                    DataMua dataMuaAdd = checkExitDataMua(dataMuas, stringDate, stringTime);
                    endData.add(dataMuaAdd);
                }
            }
            // new moi sau moi lan chay de tranh du lieu cu ton tai
            dataMuas = new ArrayList<>();
        }
        return endData;
    }

    public static List<DataMucNuoc> getUrlDataMucNuoc(List<String> listDate, List<String> listTime, String formDate, String toDate, List<InputDataMucNuoc> inputDataMucNuocList) {
        List<DataMucNuoc> dataMucNuoc = new ArrayList<>();
        List<DataMucNuoc> endData = new ArrayList<>();
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'+07:00'");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy'T'HH:mm:ss");
        Date date = null;
        for (int j = 0; j < getUrlMucNuocs(URL, formDate, toDate, inputDataMucNuocList).size(); j++) {
            result = template.getForObject(getUrlMucNuocs(URL, formDate, toDate, inputDataMucNuocList).get(j), ResponseData[].class);
            for (int i = 0; i < result[0].getData().size(); i++) {
                try {
                    date = inputFormat.parse(result[0].getData().get(i).get(0));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String formattedDate = outputFormat.format(date);
                DataMucNuoc d = new DataMucNuoc(formattedDate.split("T")[0], formattedDate.split("T")[1], result[0].getData().get(i).get(1));
                dataMucNuoc.add(d);
            }
            for (String strDate : listDate) {
                for (String strTime : listTime) {
                    DataMucNuoc datas = checkExitDataMucNuoc(dataMucNuoc, strDate, strTime);
                    endData.add(datas);
                }
            }
            dataMucNuoc = new ArrayList<>();
        }
        return endData;
    }
}

