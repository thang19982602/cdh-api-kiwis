package neo.nbd.utils;

import java.io.*;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import neo.nbd.dto.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.xml.crypto.Data;


public class WriteExcelUtils implements Runnable {

    private static CellStyle cellStyleFormatNumber = null;
    Thread t;

    WriteExcelUtils() {
        // thread created
        t = new Thread(this, "Admin Thread");
        t.start();
    }

    public static void main(String[] args) {
        new WriteExcelUtils();
    }

    public void run() {
//*link locallhost
//        String urlInput = "C:\\Users\\PC\\Desktop\\API-CDH-KIWIS\\cdh-api-kiwis\\input_and_output/input_cdh.xlsx";
//        List<InputDataHoChua> inputDataHoChuaList = ConvertData.readFileInputHoChua(urlInput);

//*link chạy exe
       String urlInputHoChua = WriteExcelUtils.class.getResource("").getPath().split("data")[0] + "/data/inputHoChua_cdh.xlsx";
        List<InputDataHoChua> inputDataHoChuaList = ConvertData.readFileInputHoChua(urlInputHoChua.split("file:/")[1]);
        getListDtoDataHoChua(inputDataHoChuaList);

        String urlInputMua = WriteExcelUtils.class.getResource("").getPath().split("data")[0] + "/data/inputMua_cdh.xlsx";
        List<InputDaTaMua> inputDataMuaList = ConvertData.readFileInputMua(urlInputMua.split("file:/")[1]);
        getListDtoDataMua(inputDataMuaList);

        String urlInputMucNuoc = WriteExcelUtils.class.getResource("").getPath().split("data")[0] + "/data/inputMucNuoc_cdh.xlsx";
        List<InputDataMucNuoc> inputDataMucNuocList = ConvertData.readFileInputMucNuoc(urlInputMucNuoc.split("file:/")[1]);
        getListDtoDataMucNuoc(inputDataMucNuocList);
        //*link chạy locallhoot
       // String urlInputHoChua = "C:\\Users\\PC\\Desktop\\API-CDH-KIWIS\\cdh-api-kiwis\\input_and_output/inputHoChua_cdh.xlsx";
//        List<InputDataHoChua> inputDataHoChuaList = ConvertData.readFileInputHoChua(urlInputHoChua);
//        getListDtoDataHoChua(inputDataHoChuaList);
//        String urlInputMua = "C:\\Users\\PC\\Desktop\\API-CDH-KIWIS\\cdh-api-kiwis\\input_and_output/inputMua_cdh.xlsx";
//        List<InputDaTaMua> inputDataMuaList = ConvertData.readFileInputMua(urlInputMua);
//        getListDtoDataMua(inputDataMuaList);
//        String urlInputMucNuoc = "C:\\Users\\PC\\Desktop\\API-CDH-KIWIS\\cdh-api-kiwis\\input_and_output/inputMucNuoc_cdh.xlsx";
//        List<InputDataMucNuoc> inputDataMucNuocList = ConvertData.readFileInputMucNuoc(urlInputMucNuoc);
//        getListDtoDataMucNuoc(inputDataMucNuocList);

        //*link chạy file exe
//        String directory = WriteExcelUtils.class.getResource("").getPath().split("data")[0] ;
//        final String excelFilePath = directory.split("file:/")[1] + "/data/output_cdh.xlsx";

//*link localhost/
        try {

            Thread.sleep( 15 *60 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new WriteExcelUtils();
    }

    public static void getListDtoDataHoChua(List<InputDataHoChua> inputDataHoChuaList) {
        int numberDate = getNumBerDateHoChua(inputDataHoChuaList);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        //lay ngay hien tai
        LocalDateTime now = LocalDateTime.now();
        //check đối tượng này sử dụng 1laanf trong hthong()
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = dtf.format(now);
        Date date = null;
        try {
            date = sdf.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<String> lstDate = new ArrayList<>();
        for (int i = numberDate - 1; i >= 0; i--) {
            //chứa ngyaf hiện tại
            calendar.setTime(date);
            //chỉ ra ngày mong muốn muốn lấy
            calendar.add(Calendar.DATE, -i);
            Date yesterday = calendar.getTime();
            lstDate.add(sdf.format(yesterday));
        }
        List<String> listTime = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            if (i < 10) {
                listTime.add("0" + i + ":00:00");
            } else {
                listTime.add(i + ":00:00");
            }
        }
        //tao list ho chua
        List<String> dates = new ArrayList<>();
        List<String> times = new ArrayList<>();
        List<String> mucNuocs = new ArrayList<>();
        List<String> qXas = new ArrayList<>();
        List<String> qDens = new ArrayList<>();

        List<DataHoChua> dataHoChuaListMucNuoc = ConvertData.getDataMucNuocAPI(lstDate, listTime, lstDate.get(0), lstDate.get(lstDate.size() - 1), inputDataHoChuaList);
        List<DataHoChua> dataHoChuaListQXa = ConvertData.getDataQXaAPI(lstDate, listTime, lstDate.get(0), lstDate.get(lstDate.size() - 1), inputDataHoChuaList);
        List<DataHoChua> dataHoChuaListQDen = ConvertData.getDataQDenAPI(lstDate, listTime, lstDate.get(0), lstDate.get(lstDate.size() - 1), inputDataHoChuaList);
        for (DataHoChua d : dataHoChuaListMucNuoc) {
            dates.add(d.getDateHoChua());
            times.add(d.getTimeHoChua());
            double str1 = Double.parseDouble(d.getValueHoChua());
            double kq = str1 * 100.0;
            String value = kq + "";
            mucNuocs.add(value);
        }
        for (DataHoChua d : dataHoChuaListQXa) {
            qXas.add(d.getValueHoChua());
        }
        for (DataHoChua d : dataHoChuaListQDen) {
            qDens.add(d.getValueHoChua());
        }
        DataImportHoChua datas = new DataImportHoChua();
        datas.setDates(dates);
        datas.setTimes(times);
        datas.setMucNuocs(mucNuocs);
        datas.setQXas(qXas);
        datas.setQDens(qDens);
        getUrlOutPutHoChua(datas, lstDate, inputDataHoChuaList);
    }

    public static void getListDtoDataMua(List<InputDaTaMua> inputDataMuaList) {
        int numberDate = getNumBerDateMua(inputDataMuaList);
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        //lay ngay hien tai
        LocalDateTime now = LocalDateTime.now();
        //check đối tượng này sử dụng 1laanf trong hthong() lấy lịch sử dụng múi giờ và ngôn ngữ hiện tại của hệ thống.
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = dtf.format(now);
        Date date = null;
        try {
            date = sdf.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<String> lstDate = new ArrayList<>();
        for (int i = numberDate - 1; i >= 0; i--) {
            //chứa ngyaf hiện tại
            calendar.setTime(date);
            //chỉ ra ngày mong muốn muốn lấy
            calendar.add(Calendar.DATE, -i);
            Date yesterday = calendar.getTime();
            lstDate.add(sdf.format(yesterday));
        }
        List<String> listTime = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            if (i < 10) {
                listTime.add("0" + i + ":00:00");
            } else {
                listTime.add(i + ":00:00");
            }
        }

        //tao list mua
        List<String> mua = new ArrayList<>();
        List<String> dateM = new ArrayList<>();
        List<String> timeM = new ArrayList<>();
        // truyeenf vao lstdate, listime,ngay bat dau,ngay hien tai,lis input
        //lay ra 1 list data object gồm đủ dữ liệu date và time (0h-24h) và value
        List<DataMua> dataHoChuaListMua = ConvertData.getDataMuaAPI(lstDate, listTime, lstDate.get(0), lstDate.get(lstDate.size() - 1), inputDataMuaList);

        for (DataMua d : dataHoChuaListMua) {
            dateM.add(d.getDateMua());
            timeM.add(d.getTimeMua());
            String strValue = d.getValueMua();
            Double value = Double.parseDouble(strValue);
            double kq = value * 100.0;
            String getKq = "" + kq;
            mua.add(getKq);
        }
        DataImportMua dataMua = new DataImportMua();
        dataMua.setDate(dateM);
        dataMua.setTime(timeM);
        dataMua.setMua(mua);
        getUrlOutPutMua(dataMua, lstDate, inputDataMuaList);

    }

    public static void getListDtoDataMucNuoc(List<InputDataMucNuoc> inputDataMucNuocList) {
        int numberDate = getNumberDateMucNuoc(inputDataMucNuocList);
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = dtf.format(now);
        Calendar calendar = Calendar.getInstance();
        Date date = null;
        try {
            date = sdf.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<String> listDates = new ArrayList<>();
        for (int i = numberDate - 1; i >= 0; i--) {
            calendar.setTime(date);
            calendar.add(Calendar.DATE, -i);
            Date yesterday = calendar.getTime();
            listDates.add(sdf.format(yesterday));
        }

        List<String> listTime = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            if (i < 10) {
                listTime.add("0" + i + ":00:00");
            } else {
                listTime.add(i + ":00:00");
            }
        }
        //tao list mua
        List<String> mucNuoc = new ArrayList<>();
        List<String> dateMucNuoc = new ArrayList<>();
        List<String> timeMucNuoc = new ArrayList<>();
        List<DataMucNuoc> dataHoChuaListMucNuoc = ConvertData.getUrlDataMucNuoc(listDates, listTime, listDates.get(0), listDates.get(listDates.size() - 1), inputDataMucNuocList);
        for (DataMucNuoc o : dataHoChuaListMucNuoc) {
            dateMucNuoc.add(o.getDateMucNuoc());
            timeMucNuoc.add(o.getTimeMucNuoc());
            String strValue = o.getValueMucNuoc();
            Double value = Double.parseDouble(strValue);
            double kq = value * 100.0;
            String getKq = "" + kq;
            mucNuoc.add(getKq);
        }
        DataImportMucNuoc dataMucNuoc = new DataImportMucNuoc();
        dataMucNuoc.setDate(dateMucNuoc);
        dataMucNuoc.setTime(timeMucNuoc);
        dataMucNuoc.setMucNuoc(mucNuoc);
        getUrlOutPutMucNuoc(dataMucNuoc, listDates, inputDataMucNuocList);
    }

    public static void getUrlOutPutHoChua(DataImportHoChua datas, List<String> lstDate, List<InputDataHoChua> inputDataHoChuaList) {
//        String directory = "C:\\Users\\PC\\Desktop\\API-CDH-KIWIS\\cdh-api-kiwis\\input_and_output/";
//        final String excelFilePath = directory + "/dataHoChua.xlsx";

        //*link chạy file exe
        String directory = WriteExcelUtils.class.getResource("").getPath().split("data")[0] ;
        final String excelFilePath = directory.split("file:/")[1] + "/data/outputHoChua_cdh.xlsx";
        try {
            writeExcelHoChua(datas, excelFilePath, directory, lstDate, inputDataHoChuaList);
            //Thread.sleep(15 * 60 * 1000);
//            new WriteExcelUtils();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void getUrlOutPutMua(DataImportMua dataMua, List<String> lstDate, List<InputDaTaMua> inputDataMuaList) {
//        String directory = "C:\\Users\\PC\\Desktop\\API-CDH-KIWIS\\cdh-api-kiwis\\input_and_output/";
//        final String excelFilePath = directory + "/dataMua.xlsx";
//*link chạy exe
        String directory = WriteExcelUtils.class.getResource("").getPath().split("data")[0] ;
        final String excelFilePath = directory.split("file:/")[1] + "/data/outputMua_cdh.xlsx";
        try {
            writeExcelMua(dataMua, excelFilePath, directory, lstDate, inputDataMuaList);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void getUrlOutPutMucNuoc(DataImportMucNuoc dataMucNuoc, List<String> lstDate, List<InputDataMucNuoc> inputDataMucNuocList) {
//        String directory = "C:\\Users\\PC\\Desktop\\API-CDH-KIWIS\\cdh-api-kiwis\\input_and_output/";
//        final String excelFilePath = directory + "/dataMucNuoc.xlsx";
//*link chạy exe
        String directory = WriteExcelUtils.class.getResource("").getPath().split("data")[0] ;
        final String excelFilePath = directory.split("file:/")[1] + "/data/outputMucNuoc_cdh.xlsx";
        try {
            writeExcelMucNuoc(dataMucNuoc, excelFilePath, directory, lstDate, inputDataMucNuocList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getNumBerDateHoChua(List<InputDataHoChua> list) {
        return list.get(0).getDayNumber().intValue();
    }

    public static int getNumBerDateMua(List<InputDaTaMua> list) {
        return list.get(0).getDayNumber().intValue();
    }

    public static int getNumberDateMucNuoc(List<InputDataMucNuoc> list) {
        return list.get(0).getDayNumber().intValue();
    }

    public static void writeExcelHoChua(DataImportHoChua datas, String excelFilePath, String directory, List<String> number, List<InputDataHoChua> inputDataHoChuaList) throws IOException {
        // Create Workbook
        Workbook workbook = getWorkbook(excelFilePath, directory);

        // Create sheet
        Sheet sheet = workbook.createSheet("Books"); // Create sheet with sheet name

        int rowIndex = 1;

        // Write header
        writeHeaderHoChua(sheet, rowIndex, datas, inputDataHoChuaList);

        // Write data
        rowIndex++;
        int numberDate = number.size();
        for (int i = 0; i < numberDate * 24; i++) {
            // Create row
            Row row = sheet.createRow(rowIndex);
            // Write data on row
            writeBookDateHoChua(datas, row, rowIndex - 2, numberDate);
            rowIndex++;
        }
        // Write footer
        //writeFooter(sheet, rowIndex);

        // Auto resize column witdth
        int numberOfColumn = sheet.getRow(1).getPhysicalNumberOfCells();
        System.out.println("======" + sheet.getRow(1).getPhysicalNumberOfCells());
        autosizeColumn(sheet, numberOfColumn);

        // Create file excel
        createOutputFile(workbook, excelFilePath, directory);
        System.out.println("Done!!!");
    }

    public static void writeExcelMua(DataImportMua dataMua, String excelFilePath, String directory, List<String> number, List<InputDaTaMua> inputDataMuaList) throws IOException {
        // Create Workbook
        Workbook workbook = getWorkbook(excelFilePath, directory);

        // Create sheet
        Sheet sheet = workbook.createSheet("Books"); // Create sheet with sheet name

        int rowIndex = 1;

        // Write header
        writeHeaderMua(sheet, rowIndex, dataMua, inputDataMuaList);

        // Write data
        rowIndex++;
        int numberDate = number.size();
        for (int i = 0; i < numberDate * 24; i++) {
            // Create row
            Row row = sheet.createRow(rowIndex);
            // Write data on row
            writeBookDateMua(dataMua, row, rowIndex - 2, numberDate);
            rowIndex++;
        }
        // Write footer
        //writeFooter(sheet, rowIndex);

        // Auto resize column witdth
        int numberOfColumn = sheet.getRow(1).getPhysicalNumberOfCells();
        System.out.println("======" + sheet.getRow(1).getPhysicalNumberOfCells());
        autosizeColumn(sheet, numberOfColumn);

        // Create file excel
        createOutputFile(workbook, excelFilePath, directory);
        System.out.println("Done!!!");
    }

    public static void writeExcelMucNuoc(DataImportMucNuoc dataMucNuoc, String excelFilePath, String directory, List<String> number, List<InputDataMucNuoc> inputDataMucNuocList) throws IOException {
        // Create Workbook
        Workbook workbook = getWorkbook(excelFilePath, directory);

        // Create sheet
        Sheet sheet = workbook.createSheet("Books"); // Create sheet with sheet name

        int rowIndex = 1;

        // Write header
        writeHeaderMucNuoc(sheet, rowIndex, dataMucNuoc, inputDataMucNuocList);

        // Write data
        rowIndex++;
        int numberDate = number.size();
        for (int i = 0; i < numberDate * 24; i++) {
            // Create row
            Row row = sheet.createRow(rowIndex);
            // Write data on row
            writeBookDateMucNuoc(dataMucNuoc, row, rowIndex - 2, numberDate);
            rowIndex++;
        }


        // Write footer
        //writeFooter(sheet, rowIndex);

        // Auto resize column witdth
        int numberOfColumn = sheet.getRow(1).getPhysicalNumberOfCells();
        System.out.println("======" + sheet.getRow(1).getPhysicalNumberOfCells());
        autosizeColumn(sheet, numberOfColumn);

        // Create file excel
        createOutputFile(workbook, excelFilePath, directory);
        System.out.println("Done!!!");
    }

    // Create workbook
    private static Workbook getWorkbook(String excelFilePath, String directory) throws IOException {
        Workbook workbook = null;

        if (excelFilePath.endsWith("xlsx")) {
            workbook = new XSSFWorkbook();
        } else if (excelFilePath.endsWith("xls")) {
            workbook = new HSSFWorkbook();
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }

        return workbook;
    }


    private static void writeHeaderHoChua(Sheet sheet, int rowIndex, DataImportHoChua datas, List<InputDataHoChua> inputDataHoChuaList) {
        CellStyle cellStyle = createStyleForHeader(sheet);
        CellStyle cellStyle1 = createStyleForHeaderHo(sheet);
        CellStyle cellStyleBorder = createStyleRight(sheet);
        Row row1 = sheet.createRow(0);
        Cell cell = row1.createCell(2);
        int firstCol = 2;
        int lastCol = 4;
        for (int i = 0; i < inputDataHoChuaList.size(); i++) {
            if (i != 0) {
                firstCol = firstCol + 3;
                lastCol = lastCol + 3;
            }

            Cell cell1 = row1.createCell(i == 0 ? 2 : firstCol);
            cell1.setCellStyle(cellStyle1);
            cell1.setCellValue(inputDataHoChuaList.get(i).getName());
            sheet.addMergedRegion(new CellRangeAddress(0, 0, i == 0 ? 2 : firstCol, i == 0 ? 4 : lastCol));
            cell1 = row1.createCell(i == 0 ? 4 : lastCol);
            cell1.setCellStyle(cellStyleBorder);
        }

        // Create cells
        Cell cell2 = row1.createCell(0);
        cell2 = row1.createCell(0);
        cell2.setCellStyle(cellStyle);
        cell2.setCellValue("Ngày");

        cell2 = row1.createCell(1);
        cell2.setCellStyle(cellStyle);
        cell2.setCellValue("Thời gian");

        Row row = sheet.createRow(rowIndex);
        int column = 2;
        int sizeHeader = inputDataHoChuaList.size();
        for (int i = 0; i < sizeHeader; i++) {

            cell = row.createCell(column++);
            cell.setCellStyle(cellStyle);
            cell.setCellValue("Mực nước");

            cell = row.createCell(column++);
            cell.setCellStyle(cellStyle);
            cell.setCellValue("Q.xả");

            cell = row.createCell(column++);
            cell.setCellStyle(cellStyle);
            cell.setCellValue("Q.đến");
        }
    }

    // Write header with format
    private static void writeHeaderMua(Sheet sheet, int rowIndex, DataImportMua datas, List<InputDaTaMua> inputDataMuaList) {
        CellStyle cellStyle = createStyleForHeader(sheet);
//        CellStyle cellStyle = createStyleForHeader(sheet);
        CellStyle cellStyle1 = createStyleForHeaderHo(sheet);
        CellStyle cellStyleBorder = createStyleRight(sheet);
        Row row1 = sheet.createRow(rowIndex);
        int firstCol = 2;
        for (int i = 0; i < inputDataMuaList.size(); i++) {
            if (i != 0) {
                firstCol = firstCol + 1;
            }
            Cell cell1 = row1.createCell(i == 0 ? 2 : firstCol);
            cell1.setCellStyle(cellStyle1);
            cell1.setCellValue(inputDataMuaList.get(i).getName());
//            sheet.addMergedRegion(new CellRangeAddress(0, 0, i == 0 ? 2 : firstCol, i == 0 ? 3 : lastCol));
//            cell1 = row1.createCell(i == 0 ? 3 : lastCol);
            cell1.setCellStyle(cellStyleBorder);
        }

        // Create cells

        Cell cell1 = row1.createCell(0);
        cell1.setCellStyle(cellStyle);
        cell1.setCellValue("Ngày");

        cell1 = row1.createCell(1);
        cell1.setCellStyle(cellStyle);
        cell1.setCellValue("giờ");

    }

    // Write data Ho chua
    private static void writeBookDateHoChua(DataImportHoChua data, Row row, int rowIndex, int numberDate) {
        if (cellStyleFormatNumber == null) {
            // Format number
            short format = (short) BuiltinFormats.getBuiltinFormat("#,##0");
            // DataFormat df = workbook.createDataFormat();
            // short format = df.getFormat("#,##0");

            //Create CellStyle
            Workbook workbook = row.getSheet().getWorkbook();
            cellStyleFormatNumber = workbook.createCellStyle();
            cellStyleFormatNumber.setDataFormat(format);
        }
        //DATE
        Cell cell = row.createCell(0);
        cell.setCellValue(data.getDates().get(rowIndex));

        cell = row.createCell(1);
        cell.setCellValue(data.getTimes().get(rowIndex));

        int number = numberDate * 24;
        int size = data.getDates().size() / number;
        int column = 2;
        for (int i = 0; i < size; i++) {
            //?
            int value = i == 0 ? rowIndex : (rowIndex + number * i);
            cell = row.createCell(column++);
            cell.setCellValue(data.getMucNuocs().get(value));

            cell = row.createCell(column++);
            cell.setCellValue(data.getQXas().get(value));

            cell = row.createCell(column++);
            cell.setCellValue(data.getQDens().get(value));
        }
    }

    // Write header with format
    private static void writeHeaderMucNuoc(Sheet sheet, int rowIndex, DataImportMucNuoc datas, List<InputDataMucNuoc> inputDataMucNuocList) {
        CellStyle cellStyle = createStyleForHeader(sheet);
//        CellStyle cellStyle = createStyleForHeader(sheet);
        CellStyle cellStyle1 = createStyleForHeaderHo(sheet);
        CellStyle cellStyleBorder = createStyleRight(sheet);
        Row row1 = sheet.createRow(rowIndex);
        int firstCol = 2;
        for (int i = 0; i < inputDataMucNuocList.size(); i++) {
            if (i != 0) {
                firstCol = firstCol + 1;
            }
            Cell cell1 = row1.createCell(i == 0 ? 2 : firstCol);
            cell1.setCellStyle(cellStyle1);
            cell1.setCellValue(inputDataMucNuocList.get(i).getName());
//            sheet.addMergedRegion(new CellRangeAddress(0, 0, i == 0 ? 2 : firstCol, i == 0 ? 3 : lastCol));
//            cell1 = row1.createCell(i == 0 ? 3 : lastCol);
            cell1.setCellStyle(cellStyleBorder);
        }

        // Create cells

        Cell cell1 = row1.createCell(0);
        cell1.setCellStyle(cellStyle);
        cell1.setCellValue("Ngày");

        cell1 = row1.createCell(1);
        cell1.setCellStyle(cellStyle);
        cell1.setCellValue("giờ");

    }


    // Write data mua
    private static void writeBookDateMua(DataImportMua data, Row row, int rowIndex, int numberDate) {
        if (cellStyleFormatNumber == null) {
            // Format number
            short format = (short) BuiltinFormats.getBuiltinFormat("#,##0");
            // DataFormat df = workbook.createDataFormat();
            // short format = df.getFormat("#,##0");

            //Create CellStyle
            Workbook workbook = row.getSheet().getWorkbook();
            cellStyleFormatNumber = workbook.createCellStyle();
            cellStyleFormatNumber.setDataFormat(format);
        }
        //DATE
        Cell cell = row.createCell(0);
        cell.setCellValue(data.getDate().get(rowIndex));

        cell = row.createCell(1);
        cell.setCellValue(data.getTime().get(rowIndex));

        int number = numberDate * 24;
        int size = data.getDate().size() / number;
        int column = 2;
        for (int i = 0; i < size; i++) {
            //? cần hỏi
            int value = i == 0 ? rowIndex : (rowIndex + number * i);
            cell = row.createCell(column++);
            cell.setCellValue(data.getMua().get(value));

        }
    }
    // Write data muc nuoc
    private static void writeBookDateMucNuoc(DataImportMucNuoc data, Row row, int rowIndex, int numberDate) {
        if (cellStyleFormatNumber == null) {
            // Format number
            short format = (short) BuiltinFormats.getBuiltinFormat("#,##0");
            // DataFormat df = workbook.createDataFormat();
            // short format = df.getFormat("#,##0");

            //Create CellStyle
            Workbook workbook = row.getSheet().getWorkbook();
            cellStyleFormatNumber = workbook.createCellStyle();
            cellStyleFormatNumber.setDataFormat(format);
        }
        //DATE
        Cell cell = row.createCell(0);
        cell.setCellValue(data.getDate().get(rowIndex));

        cell = row.createCell(1);
        cell.setCellValue(data.getTime().get(rowIndex));

        int number = numberDate * 24;
        int size = data.getDate().size() / number;
        int column = 2;
        for (int i = 0; i < size; i++) {
            //? cần hỏi
            int value = i == 0 ? rowIndex : (rowIndex + number * i);
            cell = row.createCell(column++);
            cell.setCellValue(data.getMucNuoc().get(value));

        }
    }

    // Create CellStyle for header
    private static CellStyle createStyleForHeader(Sheet sheet) {
        // Create font
        Font font = sheet.getWorkbook().createFont();
        font.setFontName("Times New Roman");
        font.setBold(true);
        font.setFontHeightInPoints((short) 14); // font size
        font.setColor(IndexedColors.WHITE.getIndex()); // text color

        // Create CellStyle
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setFillForegroundColor(IndexedColors.BLUE_GREY.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        return cellStyle;
    }

    private static CellStyle createStyleForHeaderHo(Sheet sheet) {
        // Create font
        Font font = sheet.getWorkbook().createFont();
        font.setFontName("Times New Roman");
        font.setBold(true);
        font.setFontHeightInPoints((short) 14); // font size
        font.setColor(IndexedColors.WHITE.getIndex()); // text color

        // Create CellStyle
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setFillForegroundColor(IndexedColors.DARK_YELLOW.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        return cellStyle;
    }

    private static CellStyle createStyleRight(Sheet sheet) {
        // Create font
        Font font = sheet.getWorkbook().createFont();
        font.setFontName("Times New Roman");
        font.setBold(true);
        font.setFontHeightInPoints((short) 14); // font size
        font.setColor(IndexedColors.WHITE.getIndex()); // text color

        // Create CellStyle
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setFillForegroundColor(IndexedColors.DARK_YELLOW.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setBorderRight(BorderStyle.THIN);
        return cellStyle;
    }

    // Write footer
//    private static void writeFooter(Sheet sheet, int rowIndex) {
//        // Create row
//        Row row = sheet.createRow(rowIndex);
//        Cell cell = row.createCell(COLUMN_INDEX_TOTAL, CellType.FORMULA);
//        cell.setCellFormula("SUM(E2:E6)");
//    }

    // Auto resize column width
    private static void autosizeColumn(Sheet sheet, int lastColumn) {
        for (int columnIndex = 0; columnIndex < lastColumn; columnIndex++) {
            sheet.autoSizeColumn(columnIndex);
        }
    }

    // Create output file
    private static void createOutputFile(Workbook workbook, String excelFilePath, String directory) throws IOException {
        try {
            File file = new File(directory);
            if (!file.isDirectory()) {
                file.mkdir();
            }
            File fileCheck = new File(excelFilePath);
            if (fileCheck.exists()) {
                file.delete();
            }
            OutputStream os = new FileOutputStream(excelFilePath);
            workbook.write(os);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}


