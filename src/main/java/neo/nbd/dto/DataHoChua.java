package neo.nbd.dto;

import jdk.nashorn.internal.objects.annotations.Constructor;
import lombok.AllArgsConstructor;
import lombok.ToString;

@lombok.Data
@ToString
@AllArgsConstructor
public class DataHoChua {
    private String dateHoChua;
    private String timeHoChua;
    private String valueHoChua;
}
