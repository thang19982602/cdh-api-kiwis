package neo.nbd.dto;

import lombok.Data;

import java.util.List;
@Data
public class DataImportMua {
    List<String> date;
    List<String> time;
    List<String> mua;
}
