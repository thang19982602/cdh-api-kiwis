package neo.nbd.dto;

import lombok.Data;

@Data
public class InputDataMucNuoc {
    Double dayNumber;
    String name;
    Double mucNuoc;
}
