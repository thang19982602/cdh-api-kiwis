package neo.nbd.dto;

import lombok.Data;

@Data
public class InputDataHoChua {
    Double dayNumber;
    String name;
    Double mucNuoc;
    Double qXa;
    Double qDen;
}
