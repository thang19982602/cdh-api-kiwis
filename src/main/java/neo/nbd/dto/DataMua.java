package neo.nbd.dto;

import lombok.AllArgsConstructor;
import lombok.ToString;

@lombok.Data
@AllArgsConstructor
@ToString
public class DataMua {
    private String dateMua;
    private String timeMua;
    private String valueMua;
}
