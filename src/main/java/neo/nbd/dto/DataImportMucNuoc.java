package neo.nbd.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
@Data
public class DataImportMucNuoc {
    List<String> date;
    List<String> time;
    List<String> mucNuoc;
}
