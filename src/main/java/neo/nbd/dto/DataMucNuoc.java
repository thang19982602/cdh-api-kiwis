package neo.nbd.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class DataMucNuoc {
    public String dateMucNuoc;
    public String timeMucNuoc;
    public String valueMucNuoc;
}
