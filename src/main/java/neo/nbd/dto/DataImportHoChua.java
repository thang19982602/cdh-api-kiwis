package neo.nbd.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class DataImportHoChua {
    private Map<String,String> dateTime = new HashMap<>();
    private List<String> dates = new ArrayList<>();
    private List<String> times = new ArrayList<>();
    private List<String> mucNuocs = new ArrayList<>();
    private List<String> qXas = new ArrayList<>();
    private List<String> qDens = new ArrayList<>();
}
