package neo.nbd.dto;

import lombok.Data;

@Data
public class InputDaTaMua {
    Double dayNumber;
    String name;
    Double mua;
}
