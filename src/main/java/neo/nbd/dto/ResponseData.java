package neo.nbd.dto;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class ResponseData {
    private String ts_id;
    private String rows;
    private String columns;
    List<List<String>> data;
}
